#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <pthread.h>

pthread_mutex_t mutex;
int pizza = 0;
int slice;

void *pizzaCount(void*p){
	int studentNumber = *((int*)p);
	pthread_mutex_lock(&mutex);
	if(slice <= 10){
		printf("Student %d eats slice %d from pizza %d\n", studentNumber, slice, pizza);
		slice += 1;
	}
	else {
		pizza += 1;
		if(pizza == 11){
			exit(0);
		}
		printf("Student %d orders pizza %d\n", studentNumber, pizza);
		slice = 1;
		printf("Pizza %d is delivered\n", pizza);

	}
	pthread_mutex_unlock(&mutex);
}

int main(int argc, char **argv){
	long conv = strtol(argv[1], NULL, 10);
	int student = conv;
	if(student < 2 || student > 5){
		printf("ERROR\nNumber must be between 2-5\n");
	}
	else{
		pthread_t students[student];
		while(pizza <= 10){
			for(int i = 0; i < student; i ++){
				int *arg = malloc(sizeof(*arg));
				*arg = i + 1;
				pthread_create(&students[i], NULL, pizzaCount, arg);
			}
			for(int i = 0; i < student; i++){
				pthread_join(students[i], NULL);
			}
		}
		return 0;
	}
}
			
