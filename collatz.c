#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void ChildProcess(int num1){	

	while(num1 != 1){
		if(num1 % 2 == 0){
			num1 = (num1 / 2);
		}
		else if(num1 % 2 == 1){
			num1 = (3*num1+1);
		}
		printf("%d\n", num1);
	}
	
}

void ParentProcess(){
	int status;
	wait(&status);
	printf("End\n");
	
}

int main(int argc, char **argv){

	int num1;
	long conv = strtol(argv[1], NULL, 10);
	num1 = conv;

	printf("Start\n");
	printf("%d\n", num1);

	int pid;
	pid = fork();
	if (pid == 0){
		ChildProcess(num1);
	}
	else{
		ParentProcess();
	}

}


