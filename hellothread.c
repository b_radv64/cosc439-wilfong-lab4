#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>

void *printHelloThread();

int main(int argc, char **argv){

	int NThreads;
	long conv = strtol(argv[1], NULL, 10);
	NThreads = conv;

	if(NThreads < 1 || NThreads > 10){
		printf("ERROR\nNumber must be between 1-10\n");
	}
	else{
		pthread_t thr[NThreads];
		int i, j;

		
		for(i = 0; i < NThreads; i++){
			pthread_create(&thr[i], NULL, printHelloThread, NULL);
		}

		for(j = 0; j < NThreads; j++){
			pthread_join(thr[j], NULL);
		}
	}
}

void *printHelloThread(){
	printf("Creating thread #%ld\n", pthread_self());
	printf("Hello world! I am thread #%ld\n", pthread_self());
	printf("\n");	
	pthread_exit((void *) pthread_self());

}

