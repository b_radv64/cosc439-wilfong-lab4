#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void ChildProcess(int num1){	
	char str[12];
	sprintf(str, "%d", num1);
	char *args[] ={"./main", str, NULL};
	execvp("./main", args); 
}

void ParentProcess(){
	int status;
	wait(&status);

}

int main(){

	int num1 = 0;
	printf("Enter a number between 1-100: ");
	while(num1 < 1 || num1 > 100){
		scanf("%d", &num1);
		if(num1 < 1 || num1 > 100){
			printf("Pick a number between 1-100\n");
		}
	}
	printf("You entered %d\n", num1);

	int pid;
	pid = fork();
	if (pid == 0){
		ChildProcess(num1);
	}
	else{
		ParentProcess();
	}
	

}


